package Subiecte;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class S2 extends JFrame {
    JLabel text1,text2;
    JTextField input1,input2,result;
    JButton calc;


    public S2() {


        setTitle("Calculatorul Buclucas");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }
    public void init(){

        this.setLayout(null);
        int width=120;int height = 20;

        text1 = new JLabel("Introdu primul nr:");
        text1.setBounds(40, 50, width, height);

        text2 = new JLabel("Introdu al doilea nr:");  // Am adugat si 2 text fielduri pentru a imbunatati UI-ul
        text2.setBounds(40, 100,width, height);

        input1 = new JTextField();
        input1.setBounds(150,50,width, height);

        input2 = new JTextField();
        input2.setBounds(150,100,width, height);

        calc = new JButton("Calculeaza");
        calc.setBounds(10,150,width, height);
        calc.addActionListener(new Calc());

        result = new JTextField();
        result.setBounds(10,190,140, 50);
        result.setEditable(false); // fac al treilea camp needitabil

        add(text1);add(text2);add(input1);add(input2);add(calc); add(result);

    }
    public static void main (String[] args)
    {
        S2 A = new S2();

    }

    public class Calc implements ActionListener
    {
        public void actionPerformed (ActionEvent e)
        {
            try {


                S2.this.result.setText(""); // resetez text fieldul la fiecare calcul
                String input1 = S2.this.input1.getText(); // iau inputul din primul text field
                String input2 = S2.this.input2.getText(); //iau inputul din al doilea text field
                if(input1.length()!=0 && input2.length()!=0) {


                    int nr1 = Integer.parseInt(input1);
                    int nr2 = Integer.parseInt(input2);  // le convertesc in int pentru a le putea inmulti, ulterior transfomand rezultatul inapoi in string
                    int rezultat = nr1 * nr2;
                    String s = String.valueOf(rezultat);
                    S2.this.result.setText(s); // rezultatul e transpus in string fiindca metoda seText() asteapta un string
                }
            }
            catch(NumberFormatException ignored)
            {
                System.out.println("Nu ai introdus un numar");

            }


        }


    }
}
